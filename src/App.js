import React, { useState } from "react";
import { todos as todosList } from "./todos.js";
import { v4 as uuid } from "uuid";
import { BrowserRouter as Router, Switch, Route, NavLink } from "react-router-dom";
import TodoItem from "./components/todoitem/TodoItem";
import TodoList from "./components/todolist/TodoList";

function App() {
  const [todos, setTodos] = useState(todosList);
  const [inputText, setInputText] = useState("");

  const handleAddTodo = (event) => {
    if (event.which === 13) {
      const newID = uuid();
      const newTodo = {
        userId: 1,
        id: newID,
        title: inputText,
        completed: false,
      };
      const newTodos = { ...todos };
      newTodos[newID] = newTodo;
      setTodos(newTodos);
      setInputText("");
    }
  };

  const handleToggle = (id) => {
    const newTodos = { ...todos };
    newTodos[id].completed = !newTodos[id].completed;
    setTodos(newTodos);
  };

  const handleDeleteTodo = (id) => {
    const newTodos = { ...todos };
    delete newTodos[id];
    setTodos(newTodos);
  };

  const handleClearCompletedTodos = () => {
    const newTodos = { ...todos };
    for (const todo in newTodos) {
      if (newTodos[todo].completed) {
        delete newTodos[todo];
      }
    }
    setTodos(newTodos);
  };

  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <input
          onChange={(event) => setInputText(event.target.value)}
          onKeyDown={(event) => handleAddTodo(event)}
          className="new-todo"
          value={inputText}
          placeholder="What needs to be done?"
          autofocus
        />
      </header>
      <Switch>
        <Route exact path="/">
          <TodoList todos={Object.values(todos)} handleToggle={handleToggle} handleDeleteTodo={handleDeleteTodo} />
        </Route>
        <Route path="/active">
          <TodoList
            todos={Object.values(todos).filter((todo) => !todo.completed)}
            handleToggle={handleToggle}
            handleDeleteTodo={handleDeleteTodo}
          />
        </Route>
        <Route path="/completed">
          <TodoList
            todos={Object.values(todos).filter((todo) => todo.completed)}
            handleToggle={handleToggle}
            handleDeleteTodo={handleDeleteTodo}
          />
        </Route>
      </Switch>
      <footer className="footer">
        <span className="todo-count">
          <strong>{Object.values(todos).filter((todo) => !todo.completed).length}</strong> item(s) left
        </span>

        <div>
          <nav>
            <ul className="filters">
              <li>
                <NavLink exact to="/" activeClassName="selected">
                  All
                </NavLink>
              </li>
              <li>
                <NavLink to="/active" activeClassName="selected">
                  Active
                </NavLink>
              </li>
              <li>
                <NavLink to="/completed" activeClassName="selected">
                  Completed
                </NavLink>
              </li>
            </ul>
          </nav>
        </div>
        <button className="clear-completed" onClick={() => handleClearCompletedTodos()}>
          Clear completed
        </button>
      </footer>
    </section>
  );
}

export default App;
